use sourcemap::SourceMap;
use std::fs::File;
use std::collections::HashMap;
use std::io::Read;

fn main() {

    let matches = {
        use clap::*;
        App::new("kasm source map viewer")
            .arg(Arg::with_name("SOURCE_INPUT")
                .required(true)
                .index(1))
            .arg(Arg::with_name("MAP_INPUT")
                .required(true)
                .index(2))
            .arg(Arg::with_name("COMPILED_INPUT")
                .required(true)
                .index(3))
            .get_matches()
    };

    let source_input_file_name = matches.value_of("SOURCE_INPUT").unwrap();
    let map_input_file_name = matches.value_of("MAP_INPUT").unwrap();
    let compiled_input_file_name = matches.value_of("COMPILED_INPUT").unwrap();

    let sourcemap_file = File::open(map_input_file_name).unwrap();
    let mut sourcemap = SourceMap::from_reader(sourcemap_file).unwrap();


    let mut source_file = File::open(source_input_file_name).unwrap();

    let mut source = "".to_string();
    source_file.read_to_string(&mut source).unwrap();

    sourcemap.set_source_contents(0, Some(&source));


    let mut dest_file = File::open(compiled_input_file_name).unwrap();
    let mut dest = "".to_string();
    dest_file.read_to_string(&mut dest).unwrap();

    let mut map = HashMap::new();

    for token in sourcemap.tokens() {
        let destination = token.get_dst_line();
        let source = token.get_src_line();
        map.insert(destination, source);
    }

    let view = sourcemap.get_source_view(0).unwrap();

    for (i, dest_line) in dest.lines().enumerate() {
        if let Some(src) = map.get(&(i as u32)) {
            println!("0x{:03x}: {} - line {:3}: {}", i, dest_line, src, view.get_line(*src as u32).unwrap());
        } else {
            println!("0x{:03x}: {}", i, dest_line);
        }
    }
}
