const path = require("path");
//const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");
const WorkerPlugin = require("worker-plugin");

const dist = path.resolve(__dirname, "dist");

module.exports = {
  mode: "development",

  devtool: "source-map",
  entry: {
    index: "./src/browser.js"
  },
  output: {
    path: dist,
    filename: "bundle.js"
  },
  devServer: {
    contentBase: dist,

  },
  plugins: [
    //new WasmPackPlugin({
    //  crateDirectory: __dirname + "/../komp-emulator/",
    //  //extraArgs: "--out-name index"
    //}),
    new WorkerPlugin()
  ]
};
