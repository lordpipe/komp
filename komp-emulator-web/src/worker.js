const Comlink = require("comlink");

import("../../komp-emulator/pkg").then(module => {
  try {
    Comlink.expose(module);
  } catch (err) {
    console.error(err);
  }
});
