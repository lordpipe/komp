// const loader = require("assemblyscript/lib/loader");

// const imports = {  };
// loader.instantiateStreaming(
//   fetch("emulator.wasm"),
//   imports
// ).then(module => {
//   console.log("loaded!");
// });
//

const Comlink = require("comlink");

const worker = Comlink.wrap(
  new Worker("./worker.js", { name: "worker", type: "module" }));

window.module = worker;

window.Comlink = Comlink;
