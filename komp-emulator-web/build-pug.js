const pug = require("pug");
const fs = require("fs");

fs.writeFileSync(__dirname + "/dist/index.html", pug.renderFile(__dirname + "/templates/main.pug", {
  name: "komp"
}));
