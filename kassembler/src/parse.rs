use nom::{
  branch::alt,
  bytes::complete::{tag, take, take_while},
  character::complete::{alpha1, char, digit1, hex_digit1, space0, space1, multispace1, one_of},
  combinator::{cut, map, map_res, opt, all_consuming, recognize},
  error::{context, VerboseError},
  multi::{separated_list, many1},
  sequence::{delimited, preceded, terminated, tuple},
  IResult,
};


use crate::program::*;

#[derive(Debug)]
pub enum ParsedLine {
    FnStart { name: String, args: Vec<String> },
    Label { name: String },
    Instruction(Instruction)
}

pub fn parse_line<'a>(line: &'a str) -> IResult<&'a str, Option<ParsedLine>, VerboseError<&'a str>> {
    all_consuming(
        terminated(
            preceded(space0, alt((
                    map(alt((
                        alt((
                            parse_fn,
                            parse_label,
                            parse_basic_instruction,
                            parse_call,
                            parse_stack_size,
                            parse_const,
                            parse_jumps,
                            parse_global_data,
                            parse_global_const,
                            parse_global_get,
                            parse_global_set,
                            parse_global_ptr,
                            parse_static_get,
                            parse_static_set,
                            parse_local_uninitialized,
                            parse_local_const,
                            parse_local_get,
                            parse_local_set,
                            parse_local_ptr,
                        )),
                        alt((
                            parse_offset_immediate,
                            parse_dyn_get,
                            parse_dyn_set,
                            parse_halt,
                            parse_suspend,
                            parse_register_set,
                            parse_register_get,
                            parse_heap_ptr,
                        ))
                    )), Some),
                    map(space0, |_| None))
                )
            ),
            preceded(space0, alt((parse_comment, take(0usize))))
        )
    )(line)
}


fn parse_comment<'a>(i: &'a str) -> IResult<&'a str, &'a str, VerboseError<&'a str>> {
    preceded(
        tag("//"), take_while(|_| true)
    )(i)
}

fn parse_fn<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("fn"), space1),
            tuple((
                context("function name", cut(parse_identifier)),
                delimited(tag("("),
                    separated_list(tag(","), delimited(space0, parse_identifier, space0)),
                tag(")"))
            
            ))
        ),
        |(name, args): (&str, Vec<&str>)| {
            ParsedLine::FnStart {
                name: name.to_string(),
                args: args.iter().map(|s| s.to_string()).collect()
            }
        }
    )(i)
}

fn parse_label<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            tag(":"),
            context("label", cut(parse_identifier))
        ),
        |name| {
            ParsedLine::Label { name: name.to_string() }
        }
    )(i)
}

fn parse_basic_instruction<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        alt((
            tag("noop"),
            tag("rand"),

            tag("return"),

            tag("reset_stack"),

            tag("discard"),
            tag("recover"),
            tag("copy"),
            tag("swap2"),

            tag("increment"),
            tag("decrement"),
            tag("negate"),

            alt((  // needed due to tuple size limit
                tag("bool_identity"),
                tag("bool_not"),

                tag("bit_not"),

                tag("shift_right1"),
                tag("shift_right1_logical"),
                tag("shift_left1"),

                tag("bit_population_count"),
                tag("bit_decode"),

                tag("and"),
                tag("or"),
                tag("xor"),
                tag("and_not"),

                tag("add"),
                tag("subtract"),

                tag("equal"),
                tag("not_equal"),
            ))
        )),
        |instr| {
            ParsedLine::Instruction(Instruction::new(match instr {
                "noop" => InstructionKind::Noop,
                "rand" => InstructionKind::Rand,
                "return" => InstructionKind::Return { function: None },
                "reset_stack" => InstructionKind::ResetStack,
                "discard" => InstructionKind::Discard,
                "recover" => InstructionKind::Recover,
                "copy" => InstructionKind::Copy,
                "swap2" => InstructionKind::Swap2,
                "increment" => InstructionKind::Increment,
                "decrement" => InstructionKind::Decrement,
                "negate" => InstructionKind::Negate,
                "bool_identity" => InstructionKind::BoolIdentity,
                "bool_not" => InstructionKind::BoolNot,
                "bit_not" => InstructionKind::Not,
                "shift_right1" => InstructionKind::ShiftRight1,
                "shift_right1_logical" => InstructionKind::ShiftRight1Logical,
                "shift_left1" => InstructionKind::ShiftLeft1,
                "bit_population_count" => InstructionKind::PopulationCount,
                "bit_decode" => InstructionKind::BitDecode,
                "and" => InstructionKind::And,
                "or" => InstructionKind::Or,
                "xor" => InstructionKind::Xor,
                "add" => InstructionKind::Add,
                "subtract" => InstructionKind::Subtract,
                "equal" => InstructionKind::Equal,
                "not_equal" => InstructionKind::NotEqual,
                _ => unreachable!()
            }))
        }
    )(i)
}


fn parse_call<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("call"), space1),
            context("function name", cut(parse_identifier))
            ),
        |name| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::Call {
                    function: SymbolOrPtr::Symbol(name.to_string())
                }
            ))
        }
        )(i)
}


fn parse_stack_size<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("pragma stack_size"), space1),
            context("stack size", parse_unsigned_integer)
        ),
        |size: u32| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::StackSize { size: size as u16 },
            ))
        }
        )(i)
}
fn parse_halt<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("halt"), space1),
            context("halt signal", cut(parse_integer))
            ),
        |signal| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::Halt {
                    signal
                }
            ))
        }
        )(i)
}


fn parse_suspend<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("suspend"), space1),
            context("suspend signal", cut(parse_integer))
            ),
        |signal| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::Suspend {
                    signal
                }
            ))
        }
        )(i)
}

fn parse_const<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("const"), space1),
            context("const value", parse_integer)
        ),
        |constant| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::Const { value: constant as u32 },
            ))
        }
        )(i)
}

fn parse_jumps<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        tuple((
            terminated(alt((
                tag("jump_if_zero"),
                tag("jump_if_not_zero"),
                tag("jump"),
            )), space1),
            preceded(
                tag(":"),
                context("label", cut(parse_identifier))
            )
        )),
        |(jump_instruction, label)| {
            ParsedLine::Instruction(Instruction::new(
                match jump_instruction {
                    "jump" => InstructionKind::Jump {
                        label: SymbolOrPtr::Symbol(label.to_string())
                    },
                    "jump_if_zero" => InstructionKind::JumpIfZero {
                        label: SymbolOrPtr::Symbol(label.to_string())
                    },
                    "jump_if_not_zero" => InstructionKind::JumpIfNotZero {
                        label: SymbolOrPtr::Symbol(label.to_string())
                    },
                    _ => unreachable!()
                }
            ))
        }
        )(i)
}

fn parse_global_data<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("pragma global_data"), space1),
            tuple((
                terminated(context("identifier", parse_identifier), space1),
                context("const", parse_integer)
            ))
        ),
        |(ident, constant): (&str, u32)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalData {
                    global: SymbolOrPtr::Symbol(ident.to_string()),
                    value: constant
                }))
        }
        )(i)
}

fn parse_global_const<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("global_const"), space1),
            tuple((
                terminated(context("identifier", parse_identifier), space1),
                context("const", parse_integer)
            ))
        ),
        |(ident, constant): (&str, u32)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalConst {
                    global: SymbolOrPtr::Symbol(ident.to_string()),
                    value: constant
                }))
        }
        )(i)
}

fn parse_global_get<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("global_get"), space1),
            context("global name", parse_identifier)
        ),
        |ident| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalGet {
                    global: SymbolOrPtr::Symbol(ident.to_string())
                }
            ))
        }
        )(i)
}

fn parse_global_set<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("global_set"), space1),
            tuple((
                context("global name", cut(parse_identifier)),
                opt(context("tee option", preceded(space1, tag("tee"))))
            ))
        ),
        |(global_name, tee)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalSet {
                    global: SymbolOrPtr::Symbol(global_name.to_string()),
                    tee: tee != None
                }
            ))
        }
        )(i)
}

fn parse_global_ptr<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("global_ptr"), space1),
            context("global name", parse_identifier)
        ),
        |ident| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalPtr {
                    global: SymbolOrPtr::Symbol(ident.to_string())
                }
            ))
        }
        )(i)
}

fn parse_local_uninitialized<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("pragma local_uninitialized"), space1),
            context("local name", cut(parse_identifier))
            ),
        |local_name: &str| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::LocalUninitialized {
                    local: SymbolOrPtr::Symbol(local_name.to_string())
                }
            ))
        }
        )(i)
}

fn parse_local_const<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("local_const"), space1),
            tuple((
                terminated(context("identifier", parse_identifier), space1),
                context("const", parse_integer)
            ))
        ),
        |(ident, constant): (&str, u32)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::LocalConst {
                    local: SymbolOrPtr::Symbol(ident.to_string()),
                    value: constant
                }))
        }
        )(i)
}

fn parse_local_get<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("local_get"), space1),
            context("local name", cut(parse_identifier))
            ),
        |local_name: &str| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::LocalGet {
                    local: SymbolOrPtr::Symbol(local_name.to_string())
                }
            ))
        }
        )(i)
}

fn parse_local_set<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("local_set"), space1),
            tuple((
                context("local name", cut(parse_identifier)),
                opt(context("tee option", preceded(space1, tag("tee"))))
            ))
        ),
        |(local_name, tee)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::LocalSet {
                    local: SymbolOrPtr::Symbol(local_name.to_string()),
                    tee: tee != None
                }
            ))
        }
        )(i)
}

fn parse_local_ptr<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("local_ptr"), space1),
            context("local name", cut(parse_identifier))
            ),
        |local_name: &str| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::LocalPtr {
                    local: SymbolOrPtr::Symbol(local_name.to_string())
                }
            ))
        }
        )(i)
}


fn parse_dyn_get<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        tuple((
            terminated(alt((
                tag("dyn_get_32"),
                tag("dyn_get_16"),
                tag("dyn_get_8"),
            )), space1),
            context("offset", parse_integer)
        )),
        |(instr, offset)| {
            ParsedLine::Instruction(Instruction::new(
                match instr {
                    "dyn_get_32" => InstructionKind::DynGet32 { offset: offset as u16 },
                    "dyn_get_16" => InstructionKind::DynGet16 { offset: offset as u16 },
                    "dyn_get_8" => InstructionKind::DynGet8 { offset: offset as u16 },
                    _ => unreachable!()
                }
            ))
        }
        )(i)
}

fn parse_dyn_set<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        tuple((
            terminated(alt((
                tag("dyn_set_32"),
                tag("dyn_set_16"),
                tag("dyn_set_8"),
            )), space1),
            context("offset", parse_integer),
            opt(context("tee option", preceded(space1, tag("tee"))))
        )),
        |(instr, offset, tee)| {
            ParsedLine::Instruction(Instruction::new(
                match instr {
                    "dyn_set_32" => InstructionKind::DynSet32 { offset: offset as u16, tee: tee != None },
                    "dyn_set_16" => InstructionKind::DynSet16 { offset: offset as u16, tee: tee != None },
                    "dyn_set_8" => InstructionKind::DynSet8 { offset: offset as u16, tee: tee != None },
                    _ => unreachable!()
                }
            ))
        }
        )(i)
}


fn parse_static_set<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("static_set"), space1),
            tuple((
                context("static address", cut(parse_unsigned_integer)),
                opt(context("tee option", preceded(space1, tag("tee"))))
            ))
        ),
        |(static_addr, tee)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalSet {
                    global: SymbolOrPtr::Ptr(static_addr as u16),
                    tee: tee != None
                }
            ))
        }
        )(i)
}


fn parse_register_set<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("register_set"), space1),
            tuple((
                context("register name", cut(parse_register_name)),
                opt(context("tee option", preceded(space1, tag("tee"))))
            ))
        ),
        |(register, tee)| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::RegisterSet {
                    register, tee: tee != None
                }
            ))
        }
        )(i)
}

fn parse_register_get<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("register_get"), space1),
            context("register name", cut(parse_register_name))
            ),
        |register| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::RegisterGet {
                    register
                }
            ))
        }
        )(i)
}


fn parse_register_name<'a>(i: &'a str) -> IResult<&'a str, u8, VerboseError<&'a str>> {
    map(alt((
        tag("turing"),
        tag("hopper"),
        tag("knuth"),
        tag("lovelace"),
        tag("thompson"),
        tag("ritchie"),
        tag("dijkstra"),
        tag("neumann"),
    )), |register_name| {
        match register_name {
            "turing" => 0,
            "hopper" => 1,
            "knuth" => 2,
            "lovelace" => 3,
            "thompson" => 4,
            "ritchie" => 5,
            "dijkstra" => 6,
            "neumann" => 7,
            _ => 8,
        }
    })(i)
}



fn parse_static_get<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("static_get"), space1),
            context("offset", cut(parse_unsigned_integer))
            ),
        |addr| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::GlobalGet {
                    global: SymbolOrPtr::Ptr(addr as u16)
                }
            ))
        }
        )(i)
}

fn parse_heap_ptr<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("heap_ptr"), space1),
            context("heap offset", parse_unsigned_integer)
        ),
        |offset| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::HeapPtr { offset: offset as u16 },
            ))
        }
        )(i)
}


fn parse_offset_immediate<'a>(i: &'a str) -> IResult<&'a str, ParsedLine, VerboseError<&'a str>> {
    map(
        preceded(
            terminated(tag("offset_immediate"), space1),
            context("offset", parse_integer)
        ),
        |offset| {
            ParsedLine::Instruction(Instruction::new(
                InstructionKind::OffsetImmediate { offset: offset as u16 },
            ))
        }
        )(i)
}

fn parse_unsigned_integer<'a>(i: &'a str) -> IResult<&'a str, u32, VerboseError<&'a str>> {
    context("unsigned integer", alt((
        map_res(preceded(tag("0x"), hex_digit1), |digit_str: &str| u32::from_str_radix(digit_str, 16)),
        map_res(digit1, |digit_str: &str| u32::from_str_radix(digit_str, 10)),
    )))(i)
}

fn parse_integer<'a>(i: &'a str) -> IResult<&'a str, u32, VerboseError<&'a str>> {
    context("integer", alt((
        map_res(preceded(tuple((tag("-"), space0, tag("0x"))), hex_digit1), |digit_str: &str|
            i32::from_str_radix(digit_str, 16).map(|num| -num as u32)
        ),
        map_res(preceded(tag("-"), digit1), |digit_str: &str|
            i32::from_str_radix(digit_str, 10).map(|num| -num as u32)
        ),
        parse_unsigned_integer
    )))(i)
}

fn parse_identifier<'a>(i: &'a str) -> IResult<&'a str, &'a str, VerboseError<&'a str>> {
    context("identifier", recognize(many1(
        one_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"))))(i)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test() {
        let input = "  local_get f";
        let result = parse_line(input);
        
        match result {
            Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => {
                panic!("{}", nom::error::convert_error(input, e));
            }
            _ => {}
        }

    }
}
