#![feature(backtrace)]

// This thing is HORRIBLE

use sourcemap;

use std::io::prelude::*;
use std::fs::File;
use std::collections::HashMap;

use snafu::{ResultExt, OptionExt};

mod parse;
mod program;

pub fn assemble_file(input: &str, output: &str) -> Result<(), err::Error> {
    let mut input_file = File::open(input)
        .with_context(|| err::ReadInputFile { filename: input })?;
    let compile_output = compile(&mut input_file, &input)?;

    let mut output_file = File::create(output)
        .with_context(|| err::ReadInputFile { filename: output })?;
    let mut output_source_map_file = File::create(output.to_string() + ".map")
        .with_context(|| err::ReadInputFile { filename: output.to_string() + ".map" })?;

    for word in compile_output.binary {
        writeln!(output_file, "{:08x}", word)
            .with_context(|| err::WriteOutputFile { filename: output })?;
    }

    compile_output.source_map.to_writer(&mut output_source_map_file)
        .context(err::WriteSourceMap)?;

    Ok(())
}

pub struct CompileOutput {
    binary: Vec<u32>,
    source_map: sourcemap::SourceMap,
}

pub fn compile<T: Read>(asm_input: &mut T, input_filename: &str) -> Result<CompileOutput, err::Error> {


    let parsed = parse(asm_input)?;

    let program = resolve_local_symbols(parsed);

    let flattened_program = flatten(&program);

    let optimized = post_flatten(&flattened_program)?;

    let laid_out = layout(optimized, &program);

    let source_map = {
        let mut source_map_builder = sourcemap::SourceMapBuilder::new(None);

        source_map_builder.add_source(input_filename);

        for (line, word) in laid_out.iter().enumerate() {
            if let InstructionOrData::Instruction(instruction) = word {
                if instruction.source_map != 0 {
                    source_map_builder.add(line as u32, 0, instruction.source_map - 1, 0, Some(input_filename), None);
                }
            }
        }


        source_map_builder.into_sourcemap()
    };
    

    println!("{:#?}", laid_out);

    let encoded = encode(laid_out)?;

    println!("{:#?}", source_map);

    Ok(CompileOutput {
        binary: encoded,
        source_map
    })
}

fn encode(program: FlattenedProgramWithData) -> Result<Vec<u32>, err::Error> {
    let mut enc = Vec::with_capacity(program.len());

    use program::*;
    use program::InstructionKind::*;
    for word in program {
        match word {
            InstructionOrData::Instruction(instruction) => {
                //println!("{:#?}", &instruction);
                let encoded = match instruction.kind {
                    Noop => 0x0000_0000,

                    Const { value } => 0x0100_0000 | value,
                    Rand => 0x0200_0000,

                    GlobalConst { global: SymbolOrPtr::Ptr(global), value } => 0x0300_0000 | (global as u32) << 10 | value,
                    GlobalGet { global: SymbolOrPtr::Ptr(global) } => 0x0400_0000 | global as u32,
                    GlobalSet { global: SymbolOrPtr::Ptr(global), tee } => 0x0500_0000 | global as u32 | (!tee as u32) << 16,

                    StackStart { addr } => 0x0600_0000 | addr as u32,
                    StackEnd { addr } => 0x0700_0000 | addr as u32,

                    LocalConst { local: SymbolOrPtr::Ptr(local), value } => 0x0800_0000 | (local as u32) << 16 | value,
                    LocalGet { local: SymbolOrPtr::Ptr(local) } => 0x0900_0000 | local as u32,
                    LocalSet { local: SymbolOrPtr::Ptr(local), tee } => 0x0a00_0000 | local as u32 | (!tee as u32) << 16,
                    LocalPtr { local: SymbolOrPtr::Ptr(local) } => 0x0b00_0000 | local as u32,

                    OffsetImmediate { offset } => 0x0c00_0000 | offset as u32,

                    DynGet32 { offset } => 0x0d00_0000 | offset as u32,
                    DynGet16 { offset } => 0x0e00_0000 | offset as u32,
                    DynGet8 { offset } => 0x0f00_0000 | offset as u32,

                    DynSet32 { offset, tee } => 0x1000_0000 | offset as u32 | (!tee as u32) << 16,
                    DynSet16 { offset, tee } => 0x1100_0000 | offset as u32 | (!tee as u32) << 16,
                    DynSet8 { offset, tee } => 0x1200_0000 | offset as u32 | (!tee as u32) << 16,

                    Jump { label: SymbolOrPtr::Ptr(label) } => 0x2000_0000 | label as u32,
                    JumpIfZero { label: SymbolOrPtr::Ptr(label) } => 0x2100_0000 | label as u32,
                    JumpIfNotZero { label: SymbolOrPtr::Ptr(label) } => 0x2200_0000 | label as u32,

                    LowReturn { frame_size } => 0x2300_0000 | frame_size as u32,
                    LowCall { function, frame_size, arguments } => 0x2400_0000 | function as u32 | (arguments as u32) << 12 | (frame_size as u32) << 16,

                    ResetStack => 0x3000_0000,
                    Discard => 0x3100_0000,
                    Recover => 0x3200_0000,
                    Copy => 0x3300_0000,
                    Swap2 => 0x3400_0000,

                    Increment => 0x3500_0000,
                    Decrement => 0x3600_0000,
                    Negate => 0x3700_0000,

                    BoolIdentity => 0x3800_0000,
                    BoolNot => 0x3900_0000,

                    Not => 0x3a00_0000,

                    ShiftRight1 => 0x3b00_0000,
                    ShiftRight1Logical => 0x3c00_0000,
                    ShiftLeft1 => 0x3d00_0000,

                    PopulationCount => 0x3e00_0000,
                    BitDecode => 0x3f00_0000,

                    And => 0x5000_0000,
                    Or => 0x5100_0000,
                    Xor => 0x5200_0000,

                    Add => 0x5300_0000,
                    Subtract => 0x5400_0000,

                    Equal => 0x5500_0000,
                    NotEqual => 0x5600_0000,

                    Suspend { signal } => 0xfa00_0000 | signal as u32,
                    Halt { signal } => 0xff00_0000 | signal as u32,

                    RegisterSet { register, tee } => 0xb000_0000 | (register as u32) << 16 | !tee as u32,
                    RegisterGet { register } => 0xb100_0000 | (register as u32) << 16,

                    _ => unreachable!(),
                };

                enc.push(encoded);
                
            }
            InstructionOrData::Data(data) => {
                enc.push(data);
            }
        }
    }

    Ok(enc)
}

type FlattenedProgramWithData = Vec<InstructionOrData>;

#[derive(Debug, Clone)]
enum InstructionOrData {
    Instruction(program::Instruction),
    Data(u32)
}

fn layout(FlattenedProgramWithConsts { mut program, consts }: FlattenedProgramWithConsts, original_program: &program::KasmProgram) -> FlattenedProgramWithData {
    use program::*;
    use program::InstructionKind::*;

    let globals_start = program.len();

    let mut globals = consts.iter().map(|(name, value)| (name.clone(), *value))
        .collect::<Vec<(String, u32)>>();
    globals.sort_by(|a, b| a.0.cmp(&b.0));

    globals.extend(original_program.config.globals.iter().cloned());

    let mut func_addresses = HashMap::new();
    let mut jump_labels = HashMap::new();

    for (line, (func_label, instruction)) in program.iter().enumerate() {
        if let Some(func_label) = func_label {
            func_addresses.insert(func_label.clone(), line as u16);
        }
        if let Some(label) = &instruction.label {
            jump_labels.insert(label.clone(), line as u16);
        }
    }

    for (_, instruction) in program.iter_mut() {
        match &instruction.kind {
            Call { function: SymbolOrPtr::Symbol(function) } => {
                let kasm_function = original_program.get_func_by_name(&function);
                instruction.kind = LowCall {
                    function: func_addresses[function.as_str()],
                    frame_size: kasm_function.locals.len() as u8,
                    arguments: kasm_function.arguments.len() as u8,
                }
            }
            Return { function: Some(SymbolOrPtr::Symbol(function)) } => {
                let kasm_function = original_program.get_func_by_name(&function);
                instruction.kind = LowReturn {
                    frame_size: kasm_function.locals.len() as u8
                }
            }

            Jump { label: SymbolOrPtr::Symbol(label) } => {
                instruction.kind = Jump { label: SymbolOrPtr::Ptr(jump_labels[label]) };
            }
            JumpIfZero { label: SymbolOrPtr::Symbol(label) } => {
                instruction.kind = JumpIfZero { label: SymbolOrPtr::Ptr(jump_labels[label]) };
            }
            JumpIfNotZero { label: SymbolOrPtr::Symbol(label) } => {
                instruction.kind = JumpIfNotZero { label: SymbolOrPtr::Ptr(jump_labels[label]) };
            }

            _ => {}
        };
    }

    let get_global_ptr = |global: &str| {
        if let Some(pos) = globals.iter().position(|(name, _)| *name == global) {
            SymbolOrPtr::Ptr((globals_start + pos) as u16)
        } else {
            unreachable!();
        }
    };

    for (_, instruction) in program.iter_mut() {
        match instruction.kind {
            GlobalConst { global: SymbolOrPtr::Symbol(ref global), value } => {
                instruction.kind = GlobalConst {
                    global: get_global_ptr(global), value
                }
            }
            GlobalGet { global: SymbolOrPtr::Symbol(ref  global) } => {
                instruction.kind = GlobalGet {
                    global: get_global_ptr(global)
                }
            }
            GlobalSet { global: SymbolOrPtr::Symbol(ref global), tee } => {
                instruction.kind = GlobalSet {
                    global: get_global_ptr(global), tee
                }
            }
            GlobalPtr { global: SymbolOrPtr::Symbol(ref global)} => {
                instruction.kind = Const {
                    value: match get_global_ptr(global) { SymbolOrPtr::Ptr(ptr) => ptr as u32 * 4, _ => unreachable!() }
                }
            }
            _ => {}
        }
    }

    let end_of_globals = globals_start + globals.len();

    let stack_start_instruction = &mut program[1].1;
    stack_start_instruction.kind = InstructionKind::StackStart {
        // we permit locals in the main function, and lay out the beginning of our stack
        // accordingly. why? why not
        addr: (end_of_globals + original_program.main.locals.len()) as u16 - 1u16
    };

    let stack_end_instruction = &mut program[2].1;
    stack_end_instruction.kind = InstructionKind::StackEnd {
        addr: end_of_globals as u16 + original_program.config.stack_size
    };



    let mut program = program.iter().map(|instruction| InstructionOrData::Instruction(instruction.1.clone())).collect::<Vec<_>>();


    for (_, value) in globals {
        program.push(InstructionOrData::Data(value));
    }

    program
}


#[derive(Debug, Clone)]
struct FlattenedProgramWithConsts {
    program: Vec<(Option<String>, program::Instruction)>,
    consts: std::collections::HashMap<String, u32>
}

fn post_flatten(mut program: &[(Option<String>, program::Instruction)]) -> Result<FlattenedProgramWithConsts, err::Error> {
    use program::*;
    use program::InstructionKind::*;
    let mut optimized = Vec::with_capacity(program.len());

    let mut new_globals = HashMap::new();

    for (func_label, instruction) in program.iter() {
        match instruction.kind {
            LocalConst { ref local, value } => {
                if value > 0xff_ffffu32 {
                    let const_name = format!("const::{:010}", value);
                    new_globals.insert(const_name.clone(), value);

                    optimized.push((func_label.clone(), Instruction {
                        label: instruction.label.clone(),
                        source_map: instruction.source_map,
                        kind: GlobalGet {
                            global: SymbolOrPtr::Symbol(const_name)
                        }
                    }));
                    optimized.push((None, Instruction {
                        label: None,
                        source_map: instruction.source_map,
                        kind: LocalSet {
                            local: local.clone(),
                            tee: false
                        }
                    }));
                
                } else if value > 0xffffu32 {
                    optimized.push((func_label.clone(), Instruction {
                        label: instruction.label.clone(),
                        source_map: instruction.source_map,
                        kind: Const {
                            value
                        }
                    }));
                    optimized.push((None, Instruction {
                        label: None,
                        source_map: instruction.source_map,
                        kind: LocalSet {
                            local: local.clone(),
                            tee: false
                        }
                    }));
                } else {
                    optimized.push((func_label.clone(), instruction.clone()))
                }
            }
            GlobalConst { ref global, value } => {
                if value > 0xff_ffffu32 {
                    let const_name = format!("const::{:010}", value);
                    new_globals.insert(const_name.clone(), value);
                    optimized.push((func_label.clone(), Instruction {
                        label: instruction.label.clone(),
                        source_map: instruction.source_map,
                        kind: GlobalGet {
                            global: SymbolOrPtr::Symbol(const_name)
                        }
                    }));
                    optimized.push((None, Instruction {
                        label: None,
                        source_map: instruction.source_map,
                        kind: GlobalSet {
                            global: global.clone(),
                            tee: false
                        }
                    }));
                
                } else if value > 0x3ffu32 {
                    optimized.push((func_label.clone(), Instruction {
                        label: instruction.label.clone(),
                        source_map: instruction.source_map,
                        kind: Const {
                            value
                        }
                    }));
                    optimized.push((None, Instruction {
                        label: None,
                        source_map: instruction.source_map,
                        kind: GlobalSet {
                            global: global.clone(),
                            tee: false
                        }
                    }));
                } else {
                    optimized.push((func_label.clone(), instruction.clone()))
                }
            }
            Const { value } => {
                if value > 0xff_ffffu32 {
                    let const_name = format!("const::{:010}", value);
                    new_globals.insert(const_name.clone(), value);

                    optimized.push((func_label.clone(), Instruction {
                        label: instruction.label.clone(),
                        source_map: instruction.source_map,
                        kind: GlobalGet {
                            global: SymbolOrPtr::Symbol(const_name)
                        }
                    }));
                } else {
                    optimized.push((func_label.clone(), instruction.clone()))
                }
            }
            _ => {
                optimized.push((func_label.clone(), instruction.clone()))
            }
        }
    }

    Ok(FlattenedProgramWithConsts {
        program: optimized,
        consts: new_globals
    })
}

fn flatten(program: &program::KasmProgram) -> Vec<(Option<String>, program::Instruction)> {
    use program::*;

    let mut compiled = vec![];

    let KasmProgram { config, main, functions } = (*program).clone();

    compiled.push((None, Instruction::new(InstructionKind::Noop)));
    compiled.push((None, Instruction::new(InstructionKind::StackStart {
        addr: 0x50u16
    })));
    compiled.push((None, Instruction::new(InstructionKind::StackEnd {
        addr: 0x60u16
    })));

    for instruction in main.instructions {
        compiled.push((None, instruction.clone()));
    }

    compiled.push((None, Instruction::new(InstructionKind::Halt {
        signal: 0
    })));

    for (name, function) in functions {
        let mut name = Some(name);

        for instruction in function.instructions {
            compiled.push((name.take(), instruction));
        }

        compiled.push((None, Instruction::new(InstructionKind::Halt {
            signal: 0
        })));
    }

    compiled
}

fn resolve_local_symbols(mut program: program::KasmProgram) -> program::KasmProgram {
    use program::*;
    use program::InstructionKind::*;

    let resolve_local_symbols = |current_function: &mut KasmFunction, name: Option<&str>| {
        let function_name = name.unwrap_or("").to_string();

        for instruction in current_function.instructions.iter_mut() {
            if let Some(local) = match &mut instruction.kind {
                LocalConst { local: local @ SymbolOrPtr::Symbol(_), .. } => Some(local),
                LocalGet { local: local @ SymbolOrPtr::Symbol(_), .. } => Some(local),
                LocalSet { local: local @ SymbolOrPtr::Symbol(_), .. } => Some(local),
                LocalPtr { local: local @ SymbolOrPtr::Symbol(_), .. } => Some(local),
                _ => None
            } {
                let name = if let SymbolOrPtr::Symbol(name) = local { name } else { unreachable!() };

                // get position of this local in `locals` and save as offset
                *local = SymbolOrPtr::Ptr(current_function.locals.iter()
                    .position(|local| local == name).unwrap() as u16);
            }


            if let InstructionKind::Return { ref mut function } = instruction.kind {
                *function = Some(SymbolOrPtr::Symbol(function_name.clone()));
            }
            
            if let InstructionKind::Jump { label: SymbolOrPtr::Symbol(ref mut label) } = instruction.kind {
                *label = format!("{}::{}", function_name.clone(), label);
            }
            if let InstructionKind::JumpIfZero { label: SymbolOrPtr::Symbol(ref mut label) } = instruction.kind {
                *label = format!("{}::{}", function_name.clone(), label);
            }
            if let InstructionKind::JumpIfNotZero { label: SymbolOrPtr::Symbol(ref mut label) } = instruction.kind {
                *label = format!("{}::{}", function_name.clone(), label);
            }
        }
    };

    resolve_local_symbols(&mut program.main, None);

    for (name, func) in program.functions.iter_mut() {
        resolve_local_symbols(func, Some(name));
    }

    program
}

fn parse<T: Read>(asm_input: &mut T) -> Result<program::KasmProgram, err::Error> {
    let reader = std::io::BufReader::new(asm_input);


    let mut lines = vec![];

    for (num, line) in reader.lines().enumerate() {
        let line = line.with_context(|| err::ReadInput)?;

        println!("Parsing line: {}", line);
        match parse::parse_line(&line) {
            Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => {
                let str_error = nom::error::convert_error(&line, e);
                None.context(err::ParseInput {
                    line: num, verbose: str_error
                })?;
            }
            Ok((_, mut parsed)) => {
                // store line numbers
                if let Some(parse::ParsedLine::Instruction(ref mut instruction)) = &mut parsed {
                    instruction.source_map = num as u32 + 1;
                }

                println!("{:?}", parsed);

                // only acknowledge non-empty lines
                if let Some(parsed) = parsed {
                    lines.push(parsed);
                }
            }
            _ => { panic!() }
        }
    }

    let mut program = program::KasmProgram {
        config: program::KasmProgramConfig {
            stack_size: 32,
            globals: vec![]
        },
        main: program::KasmFunction {
            locals: vec![],
            arguments: vec![],
            instructions: vec![],
        },
        functions: HashMap::new()
    };

    let mut current_function = &mut program.main;
    let mut current_function_name = "".to_string();

    let mut next_label = None;

    use parse::ParsedLine::*;
    use program::*;

    for line in lines.iter_mut() {
        match line {
            FnStart { name, args } => {
                let entry = program.functions.entry(name.to_string());
                let new_function = KasmFunction {
                    arguments: args.clone(),
                    locals: args.clone(),
                    instructions: vec![],
                };
                current_function = entry.or_insert(new_function);
                current_function_name = name.clone();
            },
            Label { name } => {
                next_label = Some(name);
            },
            Instruction(ref mut instruction) => {
                use program::InstructionKind::*;
                if let Some(label) = next_label {
                    // namespace our labels so there's no name conflicts when we handle it
                    // in the final compilation steps
                    instruction.label = Some(current_function_name.clone() + "::" + label);
                    next_label = None;
                }

                if let Some(local_name) = match &instruction.kind {
                    LocalUninitialized { local: SymbolOrPtr::Symbol(local), .. } =>
                        Some(local.clone()),
                    LocalConst { local: SymbolOrPtr::Symbol(local), .. } =>
                        Some(local.clone()),
                    LocalGet { local: SymbolOrPtr::Symbol(local), .. } =>
                        Some(local.clone()),
                    LocalSet { local: SymbolOrPtr::Symbol(local), .. } =>
                        Some(local.clone()),
                    LocalPtr { local: SymbolOrPtr::Symbol(local), .. } =>
                        Some(local.clone()),
                    _ => None
                } {
                    if !current_function.locals.contains(&local_name) {
                        current_function.locals.insert(current_function.arguments.len(), local_name);
                    }
                }

                if let Some((global_name, value)) = match &instruction.kind {
                    GlobalData { global: SymbolOrPtr::Symbol(global), value } =>
                        Some((global.clone(), *value)),
                    GlobalConst { global: SymbolOrPtr::Symbol(global), .. } =>
                        Some((global.clone(), 0)),
                    GlobalGet { global: SymbolOrPtr::Symbol(global), .. } =>
                        Some((global.clone(), 0)),
                    GlobalSet { global: SymbolOrPtr::Symbol(global), .. } =>
                        Some((global.clone(), 0)),
                    _ => None
                } {
                    if !program.config.globals.iter().any(|(name, _)| name == &global_name) {
                        program.config.globals.push((global_name, value));
                    }
                }

                // these are instructions that do not output code
                match instruction.kind {
                    StackSize { size } => {
                        program.config.stack_size = size;
                    }
                    GlobalData { .. } => {}
                    LocalUninitialized { .. } => {}
                    _ => {
                        current_function.instructions.push(instruction.clone());
                    }
                }
            }
        }
    }

    Ok(program)
}


pub mod err {
    use std::io::Error as IoError;
    use snafu::{Snafu, Backtrace};
    #[derive(Debug, Snafu)]
    #[snafu(visibility(pub(super)))]
    pub enum Error {
        #[snafu(display("Could not read assembly file {:?}", filename))]
        ReadInputFile { filename: std::path::PathBuf, source: IoError, backtrace: Backtrace },
        #[snafu(display("Could not write output file {:?}", filename))]
        WriteOutputFile { filename: std::path::PathBuf, source: IoError, backtrace: Backtrace },
        #[snafu(display("Could not read assembly"))]
        ReadInput { source: IoError, backtrace: Backtrace },
        #[snafu(display("Parse error on line {}:\n{}", line, verbose))]
        ParseInput { line: usize, verbose: String },
        #[snafu(display("Could not write sourcemap:\n{}", source))]
        WriteSourceMap { source: crate::sourcemap::Error }
    }
}
