#![feature(backtrace)]

use kassembler::*;

fn main() {
    if let Err(error) = start() {
        use colored::*;
        use std::error::Error as StdError;
        use std::backtrace::BacktraceStatus;
        use std::io::prelude::*;

        let mut stderr = std::io::stderr();

        let mut source: &dyn StdError = &error;
        let mut is_first = true;

        #[allow(unused_must_use)]
        loop {
            if is_first {
                writeln!(stderr, "{}", "An unexpected error has occured:".italic().palette(164));
            } else {
                writeln!(stderr, "{}", "\nCaused by:".italic().palette(164));
            }

            is_first = false;

            writeln!(stderr, "{} {}", "Error:".red().bold(), &source);

            if let Some(trace) = source.backtrace() {
                if let BacktraceStatus::Captured = trace.status() {
                    let trace = format!("{}", trace);
                    let mut trace_lines = trace.lines();

                    trace_lines.next();

                    let mut highlight_next = false;
                    for line in trace_lines {
                        let mut color = 241;

                        if highlight_next {
                            highlight_next = false;
                            color = 248;
                        } else if line[6..].starts_with("kas") {
                            highlight_next = true;
                            color = 248;
                        }

                        writeln!(stderr, "{}", line.palette(color));
                    }
                }
            }

            if let Some(src) = source.source() {
                source = src;
            } else {
                break;
            }
        }
    }
}

fn start() -> Result<(), err::Error> {
    let matches = {
        use clap::*;
        App::new("kasm assembler")
            .arg(Arg::with_name("INPUT")
                .required(true)
                .index(1))
            .arg(Arg::with_name("OUTPUT")
                .short("o")
                .required(true)
                .takes_value(true))
            .get_matches()
    };

    let filename = matches.value_of("INPUT").unwrap();
    assemble_file(filename, matches.value_of("OUTPUT").unwrap_or("-"))?;

    Ok(())
}
