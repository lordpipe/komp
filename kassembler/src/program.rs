use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct KasmProgram {
    pub config: KasmProgramConfig,
    pub main: KasmFunction,
    pub functions: HashMap<String, KasmFunction>
}

impl KasmProgram {
    pub fn get_func_by_name(&self, name: &str) -> &KasmFunction {
        match name {
            "" => &self.main,
            _ => &self.functions[name]
        }

    }
}

#[derive(Debug, Clone)]
pub struct KasmProgramConfig {
    pub globals: Vec<(String, u32)>,
    pub stack_size: u16,
}

#[derive(Debug, Clone)]
pub struct KasmFunction {
    pub locals: Vec<String>,
    pub arguments: Vec<String>,
    pub instructions: Vec<Instruction>
}

#[derive(Debug, Clone)]
pub struct Instruction {
    pub source_map: u32,
    pub label: Option<String>,
    pub kind: InstructionKind
}

impl Instruction {
    pub fn new(kind: InstructionKind) -> Instruction {
        Instruction {
            source_map: 0,
            label: None,
            kind
        }
    }
}

#[derive(Debug, Clone)]
pub enum InstructionKind {
    Noop,

    GlobalData { global: SymbolOrPtr, value: u32 },
    GlobalConst { global: SymbolOrPtr, value: u32 },
    GlobalGet { global: SymbolOrPtr },
    GlobalSet { global: SymbolOrPtr, tee: bool },

    LocalUninitialized { local: SymbolOrPtr },
    LocalConst { local: SymbolOrPtr, value: u32 },
    LocalGet { local: SymbolOrPtr },
    LocalSet { local: SymbolOrPtr, tee: bool },
    LocalPtr { local: SymbolOrPtr },

    OffsetImmediate { offset: u16 },

    DynGet32 { offset: u16 },
    DynSet32 { offset: u16, tee: bool },

    DynGet16 { offset: u16 },
    DynSet16 { offset: u16, tee: bool },

    DynGet8 { offset: u16 },
    DynSet8 { offset: u16, tee: bool },


    Call { function: SymbolOrPtr },
    Return { function: Option<SymbolOrPtr> },
    
    Jump { label: SymbolOrPtr },
    JumpIfZero { label: SymbolOrPtr },
    JumpIfNotZero { label: SymbolOrPtr },

    Halt { signal: u32 },
    Suspend { signal: u32 },

    RegisterGet { register: u8 },
    RegisterSet { register: u8, tee: bool },

    Const { value: u32 },
    Rand,

    ResetStack,

    Discard,
    Recover,
    Copy,
    Swap2,
    
    Increment,
    Decrement,
    Negate,

    BoolIdentity,
    BoolNot,

    Not,

    ShiftRight1,
    ShiftRight1Logical,
    ShiftLeft1,

    PopulationCount,
    BitDecode,

    And,
    Or,
    Xor,

    Add,
    Subtract,

    Equal,
    NotEqual,

    // high level representation only
    StackSize { size: u16 },
    HeapPtr { offset: u16 },
    GlobalPtr { global: SymbolOrPtr },

    // low level only
    StackStart { addr: u16 },
    StackEnd { addr: u16 },

    LowCall { function: u16, frame_size: u8, arguments: u8 },
    LowReturn { frame_size: u8 },
}

#[derive(Debug, Clone)]
pub enum SymbolOrPtr {
    Symbol(String),
    Ptr(u16)
}

impl SymbolOrPtr {
    pub fn unwrap_ptr(&self) -> u16 {
        if let SymbolOrPtr::Ptr(ptr) = self {
            *ptr
        } else {
            panic!("not a ptr yet");
        }
    }
    pub fn unwrap_symbol(&self) -> &str {
        if let SymbolOrPtr::Symbol(ptr) = self {
            ptr
        } else {
            panic!("not a symbol");
        }
    }
}
