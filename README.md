# Komp redstone computer

![Screenshot of computer](screenshots/2020-06-15_01.43.18.png)

> Copyright (c) lordpipe
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of version 3 of the GNU Affero General Public
> License as published by the Free Software Foundation.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.

This repository contains tooling and world download for my redstone computer. Required to [download modpack here](https://mega.nz/#!Buxh3bLB!qyxP4XvWcF_po_MpLS5O9Zyorvdx-vgD8JViZr4La0U) (can be imported as multimc instance)

Both the tooling and the world are available under AGPL license, version 3 only. You must publish your modified copy if you run it on a server.

`komp-emulator` is currently a work in progress and does not have a GUI

## Tool usage

```bash
kasm samples/fib.kasm -o asm
```

Place `asm` into `world/opencomputers/a8583f1d-9bec-44df-b96b-cab2f3d661e5/home/asm` to get RAM-flashing lua program to read from it

To pretty-print generated sourcemaps:

```bash
kasm-sourcemapviewer samples/fib.kasm asm.map asm
```

## Instruction set

Some caveats:

- In the machine encoding, addresses always address entire words, and are 14 bits long. When pointers are actually used by code (`dyn_*`, `stack_ptr`), pointers are multiplied by 4 so they address bytes, and are 16 bits long
- The in-memory stack is statically accessed *backwards*. A `stack_set` instruction that looks like `0x0a010001` accesses a lower address than `0x0a010000`. This is an important consideration when using `stack_ptr` to treat parts of the stack frame as buffers/arrays - the beginning of your buffer/array has a higher relative address than the end of your buffer/array.
- The length of consts in `static_immediate`, `stack_immediate`, and `immediate` are limited to 10 bits, 16 bits, and 24 bits respectively. To load-immediate larger numbers, you'll of course have to use two instructions.
- The `no-tee` bit determines whether the value is consumed by the instruction. In `dyn_*` instructions, the address is always consumed

```
0x00 - noop - do absolutely nothing
0x01 - immediate [24 num] - load a 24-bit constant onto the operand stack
0x02 - rand - generate a random 32 bit number (comes from builtin Java RNG via Bus Randomizer)
0x03 - static_immediate [14 addr] [10 num] - write a 10 bit constant to an address
0x04 - static_get (10 blank) [14 addr] - read from specified memory position
0x05 - static_set (7 blank) {no-tee} (2 blank) [14 addr] - write to specified memory position

0x06 - stack_start (10 blank) [14 addr] - sets the initial stack pointer
0x07 - stack_end (10 blank) [14 addr] - sets the stack position that will cause the computer to crash if exceeded
0x08 - stack_immediate [8 reloffset] [16 value] - put a 16 bit constant on stackposition - reloffset
0x09 - stack_get (16 blank) [8 reloffset] - reads from position stackposition - reloffset
0x0a - stack_set (7 blank) {no-tee} (8 blank) [8 reloffset] - writes positon stackposition - reloffset
0x0b - stack_ptr (16 blank) [8 reloffset] - grabs stackposition - reloffset, multiplies it by 4, and puts on operand stack

0x0c - offset_immediate (8 blank) [16 offset] - add a 16 bit constant to the number at the top of the stack, ignoring the top 16 bits. useful for fast pointer arithmetic since it's slightly faster than `increment` and `add`

0x0d - dyn_get_32 (10 blank) [14 offset] - read a full word from 16 bit address specified by the top of the operand stack, plus offset
0x0e - dyn_get_16 - read a half word from 16 bit address specified by the top of the operand stack plus offset
0x0f - dyn_get_8 - read a byte from 16 bit address specified by the top of the operand stack plus offset

0x10 - dyn_set_32 (7 blank) {no-tee} - write a full word (from n-1th position on operand stack) to specified 16 bit address plus offset
0x11 - dyn_set_16 (7 blank) {no-tee} - write a half word (from n-1th position on operand stack) to specified 16 bit address plus offset
0x12 - dyn_set_8 (7 blank) {no-tee} - write a byte (from n-1th position on operand stack) to specified 16 bit address plus offset

TODO 0x13 mem_fill
    -0 (16 blank) [16 length] // last two bits ignored
    -1 (16 blank) [16 destination]
    -2 [32 fill value]

TODO 0x14 mem_copy
    -0 (16 blank) [16 length]  // last two bits ignored
    -1 (16 blank) [16 destination address]
    -2 (16 blank) [16 source address]

TODO 0x15 mem_sequential_read (20 blank) [4 length]
    -0 (18 blank) [16 source address]  // last two bits ignored

TODO 0x16 mem_sequential_write (20 blank) [4 length]
    -0 (18 blank) [16 destination address]  // last two bits ignored

0x20 - jump (10 blank) [14 addr]
0x21 - jump_if_zero (10 blank) [14 addr]
0x22 - jump_if_not_zero (10 blank) [14 addr]
0x23 - return (16 blank) [8 localscount]
0x24 - call [8 locals count] [4 args count] [12 addr]
    (WARNING: Jump position for `call` can only be in the first 16384 bytes. Make sure that
     the beginning of all your functions are within this space)

TODO 0x25 - accept_tagged_data (8 blank) [8 compare] (8 blank) - pushes 0x01 onto the stack if the top byte exactly matches `compare`, and wipes the tag if true
TODO 0x26 - payload_equal_to_immediate [24 compare] - replaces the value with 0x01 if the bottom three bytes exactly matches `compare`

0xb0 - register_set [8 register] (15 blank) {no-tee}
0xb1 - register_get [8 register] (16 blank)

0x30 - reset_stack - put the operand stack back into 0th position (useful for cleaning up things after a jump from multiple possible locations)
0x31 - discard - pop the operand stack without reading
0x32 - recover - push the operand stack without writing
0x33 - copy - duplicate the value at the top of the operand stack
0x34 - swap2 - swap the top two values of the operand stack

0x35 - increment - increment number at top of the stack by 1
0x36 - decrement - decrement number at top of stack

0x37 - negate - flip the sign of the number

0x38 - bool_identity - set to 0x00000001 if at least one bit is 1; set to 0x00000000 if all bits are 0
0x39 - bool_not - set to 0x00000001 if all bits are 0; set to 0x00000000 if at least one bit is 1

0x3a - not - invert all bits

0x3b - shift_right1 - divide integer by 2
0x3c - shift_right1_logical - shift all bits to the right
0x3d - shift_left1 - multiply integer by 2

0x3e - bit_population_count - count bits that are 1
0x3f - bit_decode - convert 5 bit number to its corresponding bit mask

TODO 0x40 - is_negative
TODO 0x41 - is_positive
TODO 0x42 - is_gt_zero
TODO 0x43 - is_lt_zero
TODO 0x44 - is_lt_one

TODO: 0x45 sign_extend_int8 - replace the first 24 bits with the 24th bit
TODO: 0x46 sign_extend_int16 - replace the first 16 bits with the 16th bit

TODO permute_cab - turn ABC into CAB (top of operand stack ordering)
TODO permute_bca - turn ABC into BCA
TODO permute_cdab - turn ABCD into CDAB
TODO copy_double - turn AB into ABCD

0x50 - and
0x51 - or
0x52 - xor
0x53 - add
0x54 - subtract
0x55 - equal
0x56 - not_equal

0x60 shift_left
0x61 shift_right (23 blank) [signfill]


0xfa - suspend [24 suspend signal] - halt execution and send signal to periphery - letting the peripheral do whatever it wants with operand stack and memory
0xff - halt
```


I've made a semi-high level language to make programming the computer as straightforward as possible:

    pragma stack_size 0x10 // set the stack size to 16

    global_const thing 0xffffffff

    suspend 0x0001  // wait for inputs from periphery, sending sleep signal 0x0001 for good luck
    call multiply
    static_set 0x3FF // place output in memory-mapped IO

    fn multiply(x, y)
        local_const output 0


        :loop

        local_get output
        local_get x
        add
        local_set output

        local_get y
        const 1
        subtract

        local_set y

        jump_if_not_zero :loop


        local_get output

        return

    fn random_crap()
        // the 8 general-purpose registers are named after computer scientists
        register_get turing
        register_set turing

        register_get hopper
        register_set hopper

        register_get knuth
        register_set knuth

        register_get lovelace
        register_set lovelace

        register_get thompson
        register_set thompson

        register_get ritchie
        register_set ritchie

        register_get dijkstra
        register_set dijkstra

        register_get neumann
        register_set neumann

This source compiles to this program: (with sourcemaps)

    0x000: 00000000
    0x001: 05000050
    0x002: 06000060
    0x003: 03000026 - line   2: global_const thing 0xffffffff
    0x004: 04800027 - line   2: global_const thing 0xffffffff
    0x005: fa000001 - line   4: suspend 0x0001  // wait for inputs from periphery, sending sleep signal 0x0001 for good luck
    0x006: 14032009 - line   5: call multiply
    0x007: 048003ff - line   6: static_set 0x3FF // place output in memory-mapped IO
    0x008: ff000000
    0x009: 07020000 - line   9:     local_const output 0
    0x00a: 08000002 - line  14:     local_get output
    0x00b: 08000000 - line  15:     local_get x
    0x00c: 50000000 - line  16:     add
    0x00d: 09800002 - line  17:     local_set output
    0x00e: 08000001 - line  19:     local_get y
    0x00f: 01000001 - line  20:     const 1
    0x010: 51000000 - line  21:     subtract
    0x011: 09800001 - line  23:     local_set y
    0x012: 1200000a - line  25:     jump_if_not_zero :loop
    0x013: 08000002 - line  28:     local_get output
    0x014: 13000003 - line  30:     return
    0x015: ff000000
    0x016: 41000000 - line  34:     register_get turing
    0x017: 40000000 - line  35:     register_set turing
    0x018: 41010000 - line  37:     register_get hopper
    0x019: 40010000 - line  38:     register_set hopper
    0x01a: 41020000 - line  40:     register_get knuth
    0x01b: 40020000 - line  41:     register_set knuth
    0x01c: 41030000 - line  43:     register_get lovelace
    0x01d: 40030000 - line  44:     register_set lovelace
    0x01e: 41040000 - line  46:     register_get thompson
    0x01f: 40040000 - line  47:     register_set thompson
    0x020: 41050000 - line  49:     register_get ritchie
    0x021: 40050000 - line  50:     register_set ritchie
    0x022: 41060000 - line  52:     register_get dijkstra
    0x023: 40060000 - line  53:     register_set dijkstra
    0x024: 41070000 - line  55:     register_get neumann
    0x025: 40070000 - line  56:     register_set neumann
    0x026: ff000000
    0x027: ffffffff
    0x028: 00000000

OpenComputers GPU - Suspend signal is 0xfa94

    0x00 - set_resolution
        stack args:
            -0: (16 blank) [8 width] [8 height]
    0x01 - set_viewport
        stack args:
            -0: (16 blank) [8 width] [8 height]
    0x02 - write_string (7 blank) {vertical}
        stack args:
            -0 (24 blank) [8 y]     // this is intentionally not compact, to make it easy to use
            -1 (24 blank) [8 x]     // no shifts needed if we do it this way
            -2 [LE UTF-8 string, null terminated]
    0x03 - write_number {signed} [2 base] [5 width]
        stack args:
            -0 (24 blank) [8 y]
            -1 (24 blank) [8 x]
            -2 [32 number]
        bases: 00 = binary, 01 = hex, 10 = decimal
    0x04 - copy
        stack args:
            -0 (24 blank) [8 dest_y]
            -1 (24 blank) [8 y]
            -2 (24 blank) [8 dest_x]
            -3 (24 blank) [8 x]
            -4 (24 blank) [8 height]
            -5 (24 blank) [8 width]
    0x05 - fill
        stack args:
            -0 (24 blank) [8 y]
            -1 (24 blank) [8 x]
            -2 (24 blank) [8 height]
            -3 (24 blank) [8 width]
            -4 [LE UTF-8 codepoint, null terminated]
    0x06 - set_foreground_color_from_palette (4 blank) [4 palette-index]
    0x07 - set_background_color
        stack args:
            -0 (8 blank) [24 color]
    0x08 - set_background_color_from_palette (4 blank) [4 palette-index]
    0x09 - set_foreground_color
        stack args:
            -0 (8 blank) [24 color]
    0x0a - set_palette_color (4 blank) [4 palette-index]
        stack args:
            -0 (8 blank) [24 color]

    0x0b - subscribe_events (4 blank) {key down} {key up} {mouse down} {mouse up}
    0x0b - pull_event (4 timeout) {key down} {key up} {mouse down} {mouse up}
        timeout values:
            0x0 =   0.0000 seconds (only pull queued events)
            0x1 =   0.0166 seconds
            0x2 =   0.0333 seconds
            0x3 =   0.0666 seconds
            0x4 =   0.1333 seconds
            0x5 =   0.2666 seconds
            0x6 =   0.5333 seconds
            0x7 =   1.0666 seconds
            0x8 =   2.1333 seconds
            0x9 =   4.2666 seconds
            0xa =   8.5333 seconds
            0xb =  17.0666 seconds
            0xc =  34.1333 seconds
            0xd =  68.2666 seconds
            0xe = 136.5333 seconds
            0xf = forever
