local component = require "component"
local sides = require "sides"
local computer = require "computer"

local r = component.redstone




component.redstone.setOutput(sides.left, 255)
os.sleep(1/10)
component.redstone.setOutput(sides.left, 0)

local start_time = computer.uptime()

while component.redstone.getInput(sides.right) < 1 do
    os.sleep(1/20)
end

local end_time = computer.uptime()

print(end_time - start_time)
