local component = require("component")
local colors = require("colors")
local sides = require("sides")

local gpu = component.gpu

local redstone_1 = component.proxy("2a366e61-48c4-4b87-908a-ad06613b1aa3")
local redstone_2 = component.proxy("e64bd624-187e-4126-8429-7ecab7110a54")
local redstone_3 = component.proxy("e10b8407-b42e-4bf1-8521-c7b91c1f19c2")

local event_backlog = {}



local decode_le_bundle, decode_whole_bundle, return_instruction, get_current_stackargs, decode_unicode

local function main_loop()
    local width, height = gpu.getResolution()
    gpu.fill(1, 1, width, height, " ")

    while true do
        if redstone_3.getInput(sides.east) > 0 then
            local instr = decode_le_bundle(redstone_3.getBundledInput(sides.bottom))

            local opcode = instr >> 8
            local arguments = instr & 0xff

            if opcode == 0x00 then
                local stack_args, _ = get_current_stackargs()

                gpu.setResolution((stack_args & 0xff00) >> 8, stack_args & 0x00ff)

                pop_stack()
                return_instruction()

            elseif opcode == 0x01 then
                local stack_args, _ = get_current_stackargs()

                gpu.setViewport((stack_args & 0xff00) >> 8, stack_args & 0x00ff)

                pop_stack()
                return_instruction()
            elseif opcode == 0x02 then
                local vertical = (arguments & 0x1) >= 1

                local y, x = get_current_stackargs()
                pop_stack()
                pop_stack()
                local string_int, _ = get_current_stackargs()

                gpu.set(x, y, decode_unicode(string_int), vertical)

                pop_stack()
                return_instruction()
            elseif opcode == 0x03 then
                local signed = (arguments & 0x80) >= 1
                local base = (arguments & 0x60) >> 5
                local width = arguments & 0x1f

                local y, x = get_current_stackargs()
                pop_stack()
                pop_stack()
                local number, _ = get_current_stackargs()

                -- detect sign bit
                local is_negative = false
                if signed and (number & 0x80000000) >= 1 then
                    is_negative = true
                    number = -number + 0x100000000
                end

                local formatted_number

                if base == 0 then
                    local function to_binary(num,bits)
                        bits = bits or math.max(1, select(2, math.frexp(num)))
                        local t = {}

                        for b = bits, 1, -1 do
                            t[b] = math.fmod(num, 2)
                            num = math.floor((num - t[b]) // 2)
                        end

                        return table.concat(t)
                    end

                    formatted_number = to_binary(number, width)
                elseif base == 1 then
                    formatted_number = string.format("%0"..width.."x", number)
                elseif base == 2 then
                    formatted_number = string.format("%"..width.."d", number)
                end



                if signed and is_negative then
                    formatted_number = "-" .. formatted_number
                elseif signed then
                    formatted_number = " " .. formatted_number
                end


                gpu.set(x, y, formatted_number, vertical)

                pop_stack()
                return_instruction()
            elseif opcode == 0x04 then
                local dest_y, y = get_current_stackargs()
                pop_stack()
                pop_stack()
                local dest_x, x = get_current_stackargs()
                pop_stack()
                pop_stack()
                local height, width = get_current_stackargs()
                pop_stack()
                pop_stack()


                gpu.copy(x, y, width, height, dest_x, dest_y)

                return_instruction()

            elseif opcode == 0x05 then
                local y, x = get_current_stackargs()
                pop_stack()
                pop_stack()

                local height, width = get_current_stackargs()
                pop_stack()
                pop_stack()

                local string_int, _ = get_current_stackargs()
                pop_stack()

                gpu.fill(x, y, width, height, decode_unicode(string_int))

                return_instruction()
            elseif opcode == 0x06 then
                local palette_index = arguments & 0xf
                gpu.setBackground(palette_index, true)
                return_instruction()
            elseif opcode == 0x07 then
                local color, _ = get_current_stackargs()
                pop_stack()

                gpu.setBackground(color, false)
                return_instruction()
            elseif opcode == 0x08 then
                local palette_index = arguments & 0xf
                gpu.setForeground(palette_index, true)
                return_instruction()
            elseif opcode == 0x09 then
                local color, _ = get_current_stackargs()
                pop_stack()

                gpu.setForeground(color, false)
                return_instruction()
            elseif opcode == 0x0a then
                local palette_index = arguments & 0xf
                local color, _ = get_current_stackargs()
                pop_stack()

                gpu.setPaletteColor(palette_index, color)
                return_instruction()
            end
        end
        os.sleep(1/20)
    end
end

function decode_le_bundle(bundle)
    local out = 0
    for i=0,15 do
        if bundle[i] >= 1 then
            out = out + 2^i
        end
    end
    return out
end

function decode_whole_bundle(lsb, msb)
    lsb = decode_le_bundle(lsb)
    msb = decode_le_bundle(msb)
    return lsb | (msb << 16)
end

function return_instruction()
    redstone_3.setOutput(sides.west, 255)
    redstone_3.setOutput(sides.west, 0)
end

function get_current_stackargs()
    local stack_args_cur = redstone_2.getBundledInput()
    stack_args_cur = decode_whole_bundle(stack_args_cur[sides.east], stack_args_cur[sides.west])

    local stack_args_last = redstone_2.getBundledInput()
    stack_args_last = decode_whole_bundle(stack_args_last[sides.south], stack_args_last[sides.north])

    return stack_args_cur, stack_args_last

end

function pop_stack()
    redstone_1.setBundledOutput(sides.south, colors.red, 255)
    redstone_1.setBundledOutput(sides.south, colors.red, 0)
end


function decode_unicode(string_int)
    local formed_string = ""

    for i=0,3 do
        local char = string_int & 0xff

        formed_string = formed_string .. string.char(char)

        string_int = string_int >> 8
    end

    return formed_string
end

main_loop()
