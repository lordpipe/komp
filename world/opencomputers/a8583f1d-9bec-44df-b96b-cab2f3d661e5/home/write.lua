local component = require "component"
local sides = require "sides"
--local colors = require "colors"

-- bottom
local mem_write = component.proxy("ea1bf564-a90a-4482-8bf9-a08c1fe01272")
local mem_read = component.proxy("1c4e53ad-04eb-4b22-95bb-a46051b67212")
--local memAddress = component.proxy("7561f598-149c-4e02-b829-c7edcace84bd")

mem_read.setOutput(sides.south, 255)
os.sleep(4/20)
mem_read.setOutput(sides.south, 0)
os.sleep(2/20)

-- top
-- local memWordWrite = component.proxy("5ba1beab-0f2d-466e-ba22-09486851a76d")
-- local memWordRead = component.proxy("96a477e1-b6a3-45aa-8a15-95970344d8ba")

local function encode_le_bundle(num)
    local bundle = {
        [0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    }

    local cur = 0
    while num > 0 do
        local rest = math.fmod(num,2)
        if rest > 0.5 then
            bundle[cur] = 255
        end
        num = (num - rest)/2
        cur = cur + 1
    end
    return bundle
end

local function decode_le_bundle(bundle)
    local out = 0
    for i=0,15 do
        if bundle[i] >= 1 then
            out = out + 2^i
        end
    end
    return out
end

local null_bundle = {
    [0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
}

local i = 0
for line in io.lines("asm") do
    print(i..": "..line)
    local num = tonumber(line:gsub("%s+", ""), 16)
    --print(num)
    --print(require("serialization").serialize(encode_le_bundle(i)))
    mem_write.setBundledOutput({
        [sides.top] = encode_le_bundle(num & 0xFFFF),
        [sides.bottom] = encode_le_bundle((num & 0xFFFF0000) >> 16),
        [sides.north] = encode_le_bundle(i),
    })
    -- mem_write.setBundledOutput({
    --     [sides.south] = ({ [13] = 255 })
    -- })
    os.sleep(4/20)
    mem_write.setOutput(sides.south, 255)
    mem_write.setOutput(sides.south, 0)


    -- mem_write.setBundledOutput({
    --     [sides.top] = null_bundle,
    --     [sides.bottom] = null_bundle,
    --     [sides.north] = null_bundle,
    --     [sides.south] = null_bundle
    -- })

    i = i + 1
end

mem_write.setBundledOutput({
    [sides.top] = null_bundle,
    [sides.bottom] = null_bundle,
    [sides.north] = null_bundle,
    [sides.south] = null_bundle
})

string.format("%08x", 40)

--mem_write.setOutput(sides.top, 0)
--mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--os.sleep(5)

--for address=0,511 do
--    mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    --mem_word_bus.setBundledOutput(sides.top, encode_le_bundle((2^16) - 1))
--    mem_write.setOutput(sides.top, 255)
--    os.sleep(1)
--    mem_write.setOutput(sides.top, 0)
--    print("wrote "..address)
--end
--mem_write.setOutput(sides.top, 0)
--mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--os.sleep(2)
--for address=0,511 do
--    mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    os.sleep(1.0)
--    local num = decode_le_bundle(mem_word_bus.getBundledInput(sides.top))
--    print("read")
--    print("address ".. address)
--    print("read ".. num)
--    if num ~= address then
--        print("NOPE")
--        os.exit()
--    end

--    --mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--end
---- for i=0,10 do
----     mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(i))
----     mem_write.setOutput(sides.top, 255)
----     mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(4000))
----     print("wrote 4000")
----     os.sleep(1)
----     print("cleaning up")
----     mem_write.setOutput(sides.top, 0)
----     mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
----     mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
----     os.sleep(1)
---- end
