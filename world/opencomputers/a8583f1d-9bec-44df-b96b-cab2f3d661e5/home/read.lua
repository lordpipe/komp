local component = require "component"
local sides = require "sides"

local arg = {...}

local mem_write = component.proxy("ea1bf564-a90a-4482-8bf9-a08c1fe01272")
local mem_read = component.proxy("1c4e53ad-04eb-4b22-95bb-a46051b67212")

local function encode_le_bundle(num)
    local bundle = {
        [0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    }

    local cur = 0
    while num > 0 do
        rest = math.fmod(num,2)
        if rest > 0.5 then
            bundle[cur] = 255
        end
        num = (num - rest)/2
        cur = cur + 1
    end
    return bundle
end

local function decode_le_bundle(bundle)
    local out = 0
    for i=0,15 do
        if bundle[i] >= 1 then
            out = out + 2^i
        end
    end
    return out
end

local null_bundle = {
    [0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
}

--for i=0,127 do
for i=(tonumber(arg[1])),(tonumber(arg[2])) do
    mem_write.setBundledOutput({
        [sides.north] = encode_le_bundle(i)
    })

    os.sleep(10/20)

    local read = mem_read.getBundledInput()

    local lsb = decode_le_bundle(read[sides.top])
    local msb = decode_le_bundle(read[sides.bottom])
    local num = lsb | (msb << 16)

    print(i..": "..string.format("%08x", num))
end

--local i = 0
--for line in io.lines("asm") do
--    print(line)
--    local num = tonumber(line:gsub("%s+", ""), 16)
--    --print(num)
--    mem_write.setBundledOutput({
--        [sides.top] = encode_le_bundle(num & 0xFFFF),
--        [sides.bottom] = encode_le_bundle((num & 0xFFFF0000) >> 16),
--        [sides.north] = encode_le_bundle(i),
--        [sides.south] = ({ [13] = 255 })
--    })

--    i = i + 1
--    os.sleep(1/20)
--end

mem_write.setBundledOutput({
    [sides.top] = null_bundle,
    [sides.bottom] = null_bundle,
    [sides.north] = null_bundle,
    [sides.south] = null_bundle
})


--mem_write.setOutput(sides.top, 0)
--mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--os.sleep(5)

--for address=0,511 do
--    mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    --mem_word_bus.setBundledOutput(sides.top, encode_le_bundle((2^16) - 1))
--    mem_write.setOutput(sides.top, 255)
--    os.sleep(1)
--    mem_write.setOutput(sides.top, 0)
--    print("wrote "..address)
--end
--mem_write.setOutput(sides.top, 0)
--mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--os.sleep(2)
--for address=0,511 do
--    mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(address))
--    os.sleep(1.0)
--    local num = decode_le_bundle(mem_word_bus.getBundledInput(sides.top))
--    print("read")
--    print("address ".. address)
--    print("read ".. num)
--    if num ~= address then
--        print("NOPE")
--        os.exit()
--    end

--    --mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
--end
---- for i=0,10 do
----     mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(i))
----     mem_write.setOutput(sides.top, 255)
----     mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(4000))
----     print("wrote 4000")
----     os.sleep(1)
----     print("cleaning up")
----     mem_write.setOutput(sides.top, 0)
----     mem_word_bus.setBundledOutput(sides.top, encode_le_bundle(0))
----     mem_address_bus.setBundledOutput(sides.top, encode_le_bundle(0))
----     os.sleep(1)
---- end
