-- this resets redstone output if another program has terminated in a weird state

local component = require "component"
local sides = require "sides"
--local colors = require "colors"

-- bottom
local mem_write = component.proxy("ea1bf564-a90a-4482-8bf9-a08c1fe01272")
local mem_read = component.proxy("1c4e53ad-04eb-4b22-95bb-a46051b67212")
--local memAddress = component.proxy("7561f598-149c-4e02-b829-c7edcace84bd")


local null_bundle = {
    [0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
}

mem_write.setBundledOutput({
    [sides.top] = null_bundle,
    [sides.bottom] = null_bundle,
    [sides.north] = null_bundle,
    [sides.south] = null_bundle
})

