import seedrandom from "seedrandom";

const MIN_SIGNED_INT = ((1<<31)-1);

export class Computer {
    memory: Uint32Array;

    operandStack: Stack = {
        data: new Uint32Array(16),
        pos: -1
    };

    callStack = {
        data: new Uint16Array(18),
        pos: -1
    }

    inMemoryStack = {
        pointer: 0x100,
        overflow: 0x200
    };

    registers = new Uint32Array(8);

    programCounter = 0;
    state = State.running;
    haltCode = 0;
    suspendCode = 0;

    rng: seedrandom.prng;

    log: Log[] = [];
    logConfig: LogConfig;

    originalProgram = new Uint32Array(0);
    originalSeed: string;

    constructor({
        memorySize,
        rngSeed = Math.random().toString(),
        logConfig = { collapse: false, enabled: true },
    }: ComputerConfig) {
        this.memory = new Uint32Array(memorySize);
        this.rng = seedrandom(rngSeed);
        this.originalSeed = rngSeed;
        this.logConfig = logConfig;
    }

    runInstruction() {
        if (this.state === State.halted || this.programCounter > this.memory.length) {
            this.state = State.halted;
            return;
        }

        if (this.state === State.suspended) {
            return;
        }

        const programCounter = this.programCounter;
        const instruction = this.memory[programCounter];

        const opcode = instruction >>> 24;

        let nextProgramCounter = programCounter + 1;

        switch (opcode) {
            case Instruction.noop: {
                if (this.logConfig.enabled) this.log.push({
                    programCounter
                });
                break;
            }

            case Instruction.immediate: {
                const constant = 0b11111111_11111111_11111111 & instruction;

                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = constant;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: constant }
                });

                break;
            }

            case Instruction.rand: {
                const rand = this.rng.int32() - MIN_SIGNED_INT;

                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = rand;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: rand }
                });

                break;
            }

            case Instruction.staticImmediate: {
                const address = (0b11111111_11111100_00000000 & instruction) >>> 14;
                const constant = 0b00000000_00000011_11111111 & instruction;

                this.memory[address] = constant;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    memoryChanges: { [address]: constant }
                });
                break;
            }

            case Instruction.staticGet: {
                const address = 0b00000000_00111111_11111111 & instruction;

                const read = this.memory[address];
                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = read;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: read }
                });
                break;
            }

            case Instruction.staticSet: {
                const address = 0b00000000_00111111_11111111 & instruction;
                const noTee =   0b00000001_00000000_00000000 & instruction;

                const data = this.operandStack.data[this.operandStack.pos];
                if (noTee) this.operandStack.pos--;

                this.memory[address] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: noTee ? this.operandStack.pos : undefined,
                    memoryChanges: { [address]: data }
                });
                break;
            }

            case Instruction.stackStart: {
                const pointer = 0b00000000_00111111_11111111 & instruction;
                this.inMemoryStack.pointer = pointer;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newMemoryStackPointer: pointer
                });
                break;
            }

            case Instruction.stackEnd: {
                const pointer = 0b00000000_00111111_11111111 & instruction;
                this.inMemoryStack.overflow = pointer;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newMemoryStackOverflow: pointer
                });
                break;
            }

            case Instruction.stackImmediate: {
                const offset  = (0b11111111_00000000_00000000 & instruction) >> 16;
                const constant = 0b00000000_11111111_11111111 & instruction;
                
                const address = this.inMemoryStack.pointer - offset;
                
                this.memory[address] = constant;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    memoryChanges: { [address]: constant }
                });
                break;
            }

            case Instruction.stackGet: {
                const offset = 0b00000000_00000000_11111111 & instruction;
                
                const address = this.inMemoryStack.pointer - offset;
                
                const read = this.memory[address];
                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = read;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: read }
                });
                break;
            }

            case Instruction.stackSet: {
                const offset = 0b00000000_00000000_11111111 & instruction;
                const noTee  = 0b00000001_00000000_00000000 & instruction;
                
                const address = this.inMemoryStack.pointer - offset;
                
                const data = this.operandStack.data[this.operandStack.pos];
                if (noTee) this.operandStack.pos--;

                this.memory[address] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: noTee ? this.operandStack.pos : undefined,
                    memoryChanges: { [address]: data }
                });
                break;
            }

            case Instruction.stackPtr: {
                const offset = 0b00000000_00000000_11111111 & instruction;
                
                const address = (this.inMemoryStack.pointer - offset) << 2;
                
                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = address;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: address }
                });
                break;
            }

            case Instruction.offsetImmediate: {
                const offset = 0b11111111_11111111 & instruction;

                this.operandStack.data[this.operandStack.pos] += offset;
                this.operandStack.data[this.operandStack.pos] &= 0xffff;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.dynGet32: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                const read = this.memory[immediateOffset + address >>> 2];

                this.operandStack.data[this.operandStack.pos] = read;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: { [this.operandStack.pos]: read }
                });

                break;
            }

            case Instruction.dynGet16: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                let read = this.memory[immediateOffset + address >>> 2];

                read >>>= (0b10 & address) * 8;
                read &= 0xffff;

                this.operandStack.data[this.operandStack.pos] = read;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: { [this.operandStack.pos]: read }
                });

                break;
            }

            case Instruction.dynGet8: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                let read = this.memory[immediateOffset + address >>> 2];

                read >>>= (0b11 & address) * 8;
                read &= 0xff;

                this.operandStack.data[this.operandStack.pos] = read;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: { [this.operandStack.pos]: read }
                });

                break;
            }

            case Instruction.dynSet32: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;
                const noTee           = 0b00000001_00000000_00000000 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                const data = this.operandStack.data[this.operandStack.pos - 1];

                this.memory[immediateOffset + address >>> 2] = data;

                this.operandStack.pos--;
                if (noTee) this.operandStack.pos--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    memoryChanges: { [immediateOffset + address >>> 2]: data }
                });

                break;
            }

            case Instruction.dynSet16: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;
                const noTee           = 0b00000001_00000000_00000000 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                let data = this.memory[immediateOffset + address / 4];
                const newData = this.operandStack.data[this.operandStack.pos - 1];

                data &= ~(0xffff << ((address & 0b10) * 8));
                data |= (newData & 0xffff) << ((address & 0b10) * 8)

                this.memory[immediateOffset + address >>> 2] = data;

                this.operandStack.pos--;
                if (noTee) this.operandStack.pos--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    memoryChanges: { [immediateOffset + address >>> 2]: data }
                });

                break;
            }

            case Instruction.dynSet8: {
                const immediateOffset = 0b00000000_00111111_11111111 & instruction;
                const noTee           = 0b00000001_00000000_00000000 & instruction;

                const address = this.operandStack.data[this.operandStack.pos];

                let data = this.memory[immediateOffset + address / 4];
                const newData = this.operandStack.data[this.operandStack.pos - 1];

                data &= ~(0xff << ((address & 0b11) * 8));
                data |= (newData & 0xff) << ((address & 0b11) * 8)

                this.memory[immediateOffset + address >>> 2] = data;

                this.operandStack.pos--;
                if (noTee) this.operandStack.pos--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    memoryChanges: { [immediateOffset + address >>> 2]: data }
                });

                break;
            }

            case Instruction.jump: {
                const address = 0b00000000_00111111_11111111 & instruction;

                nextProgramCounter = address;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newProgramCounter: address
                });
                break;
            }

            case Instruction.jumpIfZero: {
                const address = 0b00000000_00111111_11111111 & instruction;

                const isZero = this.operandStack.data[this.operandStack.pos] === 0;
                this.operandStack.pos--;

                if (isZero) nextProgramCounter = address;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newProgramCounter: isZero ? address : undefined,
                    newOperandStackPointer: this.operandStack.pos,
                });
                break;
            }

            case Instruction.jumpIfNotZero: {
                const address = 0b00000000_00111111_11111111 & instruction;

                const isNotZero = this.operandStack.data[this.operandStack.pos] !== 0;
                this.operandStack.pos--;

                if (isNotZero) nextProgramCounter = address;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newProgramCounter: isNotZero ? address : undefined,
                    newOperandStackPointer: this.operandStack.pos,
                });
                break;
            }

            case Instruction.ret: {
                const localsCount = 0b00000000_00000000_11111111 & instruction;

                this.inMemoryStack.pointer -= localsCount;

                const returnAddress = this.callStack.data[this.callStack.pos];
                nextProgramCounter = returnAddress;

                this.callStack.pos--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newProgramCounter: returnAddress,
                    newCallStackPointer: this.callStack.pos
                });

                break;
            }

            case Instruction.call: {
                const address =      0b00000000_00001111_11111111 & instruction;
                const argsCount =   (0b00000000_11110000_00000000 & instruction) >> 12;
                const localsCount = (0b11111111_00000000_00000000 & instruction) >> 16;

                this.inMemoryStack.pointer += localsCount;

                const returnAddress = programCounter + 1;
                this.callStack.pos++;
                this.callStack.data[this.callStack.pos] = returnAddress;

                const memoryChanges: { [address: number]: number } = {};

                for (let i = 0; i < argsCount; i++) {
                    const address = this.inMemoryStack.pointer - i;

                    const data = this.operandStack.data[this.operandStack.pos];
                    this.operandStack.pos--;

                    this.memory[address] = data;
                    if (this.logConfig.enabled) memoryChanges[address] = data;
                }

                nextProgramCounter = address;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newProgramCounter: address,
                    newOperandStackPointer: this.operandStack.pos,
                    newCallStackPointer: this.callStack.pos,
                    callStackChanges: { [this.callStack.pos]: returnAddress },
                    memoryChanges
                });

                break;
            }

            case Instruction.acceptTaggedData: {
                const compare = (0b11111111_00000000 & instruction) >> 8;

                const data = this.operandStack.data[this.operandStack.pos];
                const tag = (0b11111111_00000000_00000000_00000000 & data) >>> 24;

                const equal = +(tag === compare);

                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = equal;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: equal }
                });
                
                break;
            }

            case Instruction.acceptTaggedData: {
                const compare = (0b11111111_00000000 & instruction) >> 8;

                const data = this.operandStack.data[this.operandStack.pos];
                const tag = (0b11111111_00000000_00000000_00000000 & data) >>> 24;

                const equal = +(tag === compare);

                const operandStackChanges = { [this.operandStack.pos - 1]: equal };

                if (equal) {
                    const detagged = 0b11111111_11111111_11111111 & data;
                    operandStackChanges[this.operandStack.pos] = detagged;
                    this.operandStack.data[this.operandStack.pos] = detagged;
                }

                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = equal;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges
                });
                
                break;
            }
            case Instruction.payloadEqualToImmediate: {
                const compare = 0b11111111_11111111_11111111 & instruction;

                const data = this.operandStack.data[this.operandStack.pos];
                const payload = 0b11111111_11111111_11111111 & data;

                const equal = +(payload === compare);

                this.operandStack.data[this.operandStack.pos] = equal;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: { [this.operandStack.pos]: equal }
                });
                
                break;
            }

            case Instruction.registerSet: {
                const registerName = (0b11111111_00000000_00000000 & instruction) >> 16;
                const noTee =         0b00000000_00000000_00000001 & instruction;

                const data = this.operandStack.data[this.operandStack.pos];
                if (noTee) this.operandStack.pos--;

                this.registers[registerName] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: noTee ? this.operandStack.pos : undefined,
                    registerChanges: { [registerName]: data }
                });

                break;
            }

            case Instruction.registerGet: {
                const registerName = (0b11111111_00000000_00000000 & instruction) >> 16;

                const data = this.registers[registerName];

                this.operandStack.pos++;
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: { [this.operandStack.pos]: data }
                });
                break;
            }

            case Instruction.resetStack: {
                this.operandStack.pos = -1;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                });

                break;
            }
            case Instruction.discard: {
                this.operandStack.pos--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                });

                break;
            }

            case Instruction.recover: {
                this.operandStack.pos++;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                });

                break;
            }

            case Instruction.copy: {
                const newPos = this.operandStack.pos + 1;

                this.operandStack.data.copyWithin(newPos, this.operandStack.pos, this.operandStack.pos);
                this.operandStack.pos = newPos;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: newPos,
                    operandStackChanges: { [newPos]: this.operandStack.data[newPos] }
                });

                break;
            }

            case Instruction.swap2: {
                const a = this.operandStack.data[this.operandStack.pos];
                const b = this.operandStack.data[this.operandStack.pos - 1];

                this.operandStack.data[this.operandStack.pos] = b;
                this.operandStack.data[this.operandStack.pos - 1] = a;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: b,
                        [this.operandStack.pos - 1]: a
                    }
                });

                break;
            }

            case Instruction.increment: {
                this.operandStack.data[this.operandStack.pos]++;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.decrement: {
                this.operandStack.data[this.operandStack.pos]--;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.negate: {
                let num = this.operandStack.data[this.operandStack.pos];
                num = -num;
                this.operandStack.data[this.operandStack.pos] = num;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: num,
                    }
                });

                break;
            }

            case Instruction.boolIdentity: {
                let bool = this.operandStack.data[this.operandStack.pos];
                bool = +!!bool;
                this.operandStack.data[this.operandStack.pos] = bool;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: bool,
                    }
                });

                break;
            }

            case Instruction.boolNot: {
                let bool = this.operandStack.data[this.operandStack.pos];
                bool = +!bool;
                this.operandStack.data[this.operandStack.pos] = bool;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: bool,
                    }
                });

                break;
            }

            case Instruction.not: {
                let data = this.operandStack.data[this.operandStack.pos];
                data = ~data;
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.shiftRight1: {
                let data = this.operandStack.data[this.operandStack.pos];
                data >>= 1;
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.shiftRight1Logical: {
                let data = this.operandStack.data[this.operandStack.pos];
                data >>>= 1;
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.shiftLeft1: {
                let data = this.operandStack.data[this.operandStack.pos];
                data <<= 1;
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.bitPopulationCount: {
                let data = this.operandStack.data[this.operandStack.pos];
                data = bitCount(data);
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.bitDecode: {
                let data = this.operandStack.data[this.operandStack.pos];
                data = 1 << (data % 32); // this instruction ignores the top 27 bits
                this.operandStack.data[this.operandStack.pos] = data;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    operandStackChanges: {
                        [this.operandStack.pos]: data,
                    }
                });

                break;
            }

            case Instruction.and: {
                const other = this.operandStack.data[this.operandStack.pos];

                this.operandStack.pos--;

                this.operandStack.data[this.operandStack.pos]&= other;


                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.or: {
                const other = this.operandStack.data[this.operandStack.pos];

                this.operandStack.pos--;

                this.operandStack.data[this.operandStack.pos]|= other;


                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.xor: {
                const other = this.operandStack.data[this.operandStack.pos];

                this.operandStack.pos--;

                this.operandStack.data[this.operandStack.pos]^= other;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.add: {
                const other = this.operandStack.data[this.operandStack.pos];

                this.operandStack.pos--;

                this.operandStack.data[this.operandStack.pos] += other;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.subtract: {
                const other = this.operandStack.data[this.operandStack.pos];

                this.operandStack.pos--;

                this.operandStack.data[this.operandStack.pos] -= other;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.equal: {
                const left = this.operandStack.data[this.operandStack.pos];
                this.operandStack.pos--;
                const right = this.operandStack.data[this.operandStack.pos];

                this.operandStack.data[this.operandStack.pos] = +(left === right);

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.notEqual: {
                const left = this.operandStack.data[this.operandStack.pos];
                this.operandStack.pos--;
                const right = this.operandStack.data[this.operandStack.pos];

                this.operandStack.data[this.operandStack.pos] = +(left !== right);

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newOperandStackPointer: this.operandStack.pos,
                    operandStackChanges: {
                        [this.operandStack.pos]: this.operandStack.data[this.operandStack.pos],
                    }
                });

                break;
            }

            case Instruction.suspend: {
                this.suspendCode = 0b11111111_11111111_11111111 & instruction;

                this.state = State.suspended;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newState: this.state
                });
                break;
            }

            case Instruction.halt: {
                this.haltCode = 0b11111111_11111111_11111111 & instruction;

                this.state = State.halted;

                if (this.logConfig.enabled) this.log.push({
                    programCounter,
                    newState: this.state
                });
                break;
            }

            default: {
                this.state = State.halted;
            }
        }

        this.programCounter = nextProgramCounter;
    }

    runUntilHalt(maximum: number = Infinity): number | undefined {
        if (this.state === State.suspended) this.state = State.running;
        while (this.state === State.running && maximum > 0) {
            this.runInstruction();
            if (this.state === +State.suspended) {
                return this.suspendCode; 
            }
            maximum--;
        }

        return undefined;
    }


    test() {
        enum Blah {
            foo = 1,
            bar = 2,
        }
        let largeObject = {
            something: Blah.foo
        };

        if (largeObject.something === Blah.foo) {
            largeObject.something = Blah.bar;

            if (largeObject.something === Blah.bar) {

            }
        }

    }

    collapseLog() {
        const newLog: Log = {
            programCounter: this.programCounter
        };

        for (let log of this.log) {
            newLog.programCounter = log.programCounter;
            if (newLog.newProgramCounter != null) newLog.newProgramCounter = log.newProgramCounter;
            if (newLog.newState != null) newLog.newState = log.newState;

            if (newLog.newMemoryStackPointer != null) newLog.newMemoryStackPointer = log.newMemoryStackPointer;
            if (newLog.newMemoryStackOverflow != null) newLog.newMemoryStackOverflow = log.newMemoryStackOverflow;

            if (newLog.memoryChanges != null) newLog.memoryChanges = { ...newLog.memoryChanges, ...log.memoryChanges };

            if (newLog.newOperandStackPointer != null) newLog.newOperandStackPointer = log.newOperandStackPointer;
            if (newLog.operandStackChanges != null) newLog.operandStackChanges = { ...newLog.operandStackChanges, ...log.operandStackChanges };

            if (newLog.newCallStackPointer != null) newLog.newCallStackPointer = log.newCallStackPointer;
            if (newLog.callStackChanges != null) newLog.callStackChanges = { ...newLog.callStackChanges, ...log.callStackChanges };

            if (newLog.registerChanges != null) newLog.registerChanges = { ...newLog.registerChanges, ...log.registerChanges };
        } 
    }

    zero() {
        this.memory.fill(0);
        this.operandStack.data.fill(0);
        this.operandStack.pos = 0;
        this.callStack.data.fill(0);
        this.callStack.pos = 0;
        this.registers.fill(0);
        this.programCounter = 0;
        this.inMemoryStack.pointer = 0x100;
        this.inMemoryStack.overflow = 0x200;
    }

    randomize(reseed: boolean = false) {
        if (reseed) this.originalSeed = Math.random().toString();
        this.rng = seedrandom(this.originalSeed);

        const randomizeBuffer = (buffer: Uint32Array) => {
            for (let i = 0; i < buffer.length; i++) {
                buffer[i] = this.rng.int32();
            }
        }

        randomizeBuffer(this.memory);
        randomizeBuffer(this.operandStack.data);
        randomizeBuffer(this.registers);

        for (let i = 0; i < this.callStack.data.length; i++) {
            this.callStack.data[i] = this.rng.int32();
        }
    }

    flashProgram(program: Uint32Array) {
        this.originalProgram = program;
        this.memory.set(program, 0)
    }
}

enum Instruction {
    noop = 0x00,
    immediate = 0x01,
    rand = 0x02,

    staticImmediate = 0x03,
    staticGet = 0x04,
    staticSet = 0x05,

    stackStart = 0x06,
    stackEnd = 0x07,

    stackImmediate = 0x08,
    stackGet = 0x09,
    stackSet = 0x0a,

    stackPtr = 0x0b,

    offsetImmediate = 0x0c,

    dynGet32 = 0x0d,
    dynGet16 = 0x0e,
    dynGet8 = 0x0f,

    dynSet32 = 0x10,
    dynSet16 = 0x11,
    dynSet8 = 0x12,

    jump = 0x20,
    jumpIfZero = 0x21,
    jumpIfNotZero = 0x22,

    ret = 0x23,
    call = 0x24,

    acceptTaggedData = 0x25,
    payloadEqualToImmediate = 0x26,

    registerSet = 0xb0,
    registerGet = 0xb1,
    

    resetStack = 0x30,
    discard = 0x31,
    recover = 0x32,
    copy = 0x33,
    swap2 = 0x34,

    increment = 0x35,
    decrement = 0x36,

    negate = 0x37,

    boolIdentity = 0x38,
    boolNot = 0x39,

    not = 0x3a,

    shiftRight1 = 0x3b,
    shiftRight1Logical = 0x3c,
    shiftLeft1 = 0x3d,

    bitPopulationCount = 0x3e,
    bitDecode = 0x3f,
    

    and = 0x50,
    or = 0x51,
    xor = 0x52,
    add = 0x53,
    subtract = 0x54,
    equal = 0x55,
    notEqual = 0x56,

    suspend = 0xfa,
    halt = 0xff,
}

enum State {
    running,
    halted,
    suspended,
}

interface Stack {
    data: Uint32Array;
    pos: number;
}

/// Allows us to keep track of exactly what each instruction does to the state of the computer
interface Log {
    programCounter: number;

    newProgramCounter?: number;
    newState?: State;

    newMemoryStackPointer?: number;
    newMemoryStackOverflow?: number;

    memoryChanges?: { [address: number]: number };

    newOperandStackPointer?: number;
    operandStackChanges?: { [address: number]: number };

    newCallStackPointer?: number;
    callStackChanges?: { [address: number]: number };

    registerChanges?: { [address: number]: number };
}

export interface LogConfig {
    enabled: boolean;
    collapse: boolean;
}

export interface ComputerConfig {
    memorySize: number;
    rngSeed?: string;
    logConfig?: LogConfig;
}

function bitCount(n: number) {
    // bit twiddling hacks https://graphics.stanford.edu/~seander/bithacks.html
    n = n - ((n >> 1) & 0x55555555)
    n = (n & 0x33333333) + ((n >> 2) & 0x33333333)
    return ((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24
}
