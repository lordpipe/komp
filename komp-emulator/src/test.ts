import "mocha";
import { expect } from "chai";

import * as emulator from "./";

describe("emulator", () => {
    it("should set stack pointer", () => {
        const computer = new emulator.Computer({
            memorySize: 4096
        });

        computer.flashProgram(new Uint32Array([
            0,
            0x06000040,
            0x07000050,
            0xff000000
        ]))

        computer.runUntilHalt();

        expect(computer.inMemoryStack).to.deep.eq({
            pointer: 0x40,
            overflow: 0x50
        });
    });
    it("should load constants", () => {
        const computer = new emulator.Computer({
            memorySize: 4096
        });

        computer.flashProgram(new Uint32Array([
            0,
            0x06000040,
            0x07000050,
            0x0173f2ad
        ]))

        computer.runUntilHalt();

        expect(computer.operandStack.data[0]).to.eq(0x73f2ad);
    });
    it("should compute fibonacci numbers", () => {
        const computer = prepareComputer(`
            00000000
            06000015
            07000056
            0100002d
            24031007
            05010090
            ff000000
            08010001
            08020001
            09000002
            09000001
            53000000
            09000002
            0a010001
            0a010002
            09000000
            36000000
            0a000000
            22000009
            09000002
            23000003
            ff000000
        `, {
            memorySize: 4096,
        }, {
            randomize: true
        })

        computer.runUntilHalt();

        console.log(computer);

        console.log(computer.memory[0x90])
        expect(computer.memory[0x90]).to.eq(2971215073);
    });
});



function prepareComputer(program: string, config: emulator.ComputerConfig, testConfig: {
    randomize?: boolean
}) {
    const parsedProgram = new Uint32Array(program
        .split("\n")
        .map(line => parseInt(line, 16))
        .filter(val => !isNaN(val)));

    const computer = new emulator.Computer(config);
    if (testConfig.randomize) computer.randomize();
    computer.flashProgram(parsedProgram);

    return computer;
}
